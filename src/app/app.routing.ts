import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutesActivate, LoginActivate, AdminRoutes, ClientRoutes } from './shared/services/auth.service';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';

export const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full',
	},
	{
		path: 'login',
		canActivate: [LoginActivate],
		loadChildren: './components/login/login.module#LoginModule'
	},
	{
		path: '',
		component: FullLayoutComponent,
		canActivate: [RoutesActivate],
		data: {
			title: 'Home'
		},
		children: [
			{
				path: 'dashboard',
				loadChildren: './dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'advice',
				loadChildren: './components/advice/advice.module#AdviceModule'
			},
			{
				path: 'publicity',
				loadChildren: './components/publicity/publicity.module#PublicityModule'
			},
			{
				path: 'rate',
				loadChildren: './components/rate/rate.module#RateModule'
			},
			{
				path: 'zone',
				loadChildren: './components/zone/zone.module#ZoneModule'
			},
			{
				path: 'notification',
				loadChildren: './components/notification/notification.module#NotificationModule'
			},
			{
				path: 'version',
				loadChildren: './components/version/version.module#VersionModule'
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
