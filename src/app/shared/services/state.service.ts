import { Injectable } from '@angular/core';
import { APIService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StateService {
    private endpoint: string;

	constructor(private api: APIService) {
        this.endpoint = 'state';
    }

    getAll(): Observable<any> {
        return this.api.get(this.endpoint);
    };

    // save(route: any): Observable<any> {
    //     return this.api.post(this.endpoint, route);
    // };
}
