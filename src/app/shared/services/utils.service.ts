import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {
	constructor() { }

	public toUISelectI(data: Array<any>, label: any = 'name', value: string = 'id'): Array<any> {
		return data.map((d) => {
			let obj: any = {};
			obj.id = d[value];
			if (typeof label === 'string') {
				obj.text = d[label];
			} else if (Array.isArray(label)) {
				obj.text = '';
				label.forEach((l, i) => {
					if (i % 2 === 0) {
						if (l.split('.').length === 2) {
							let props = l.split('.');

							if (d.hasOwnProperty(props[0]) && typeof d[props[0]] === 'object' && d[props[0]].hasOwnProperty(props[1])) {
								obj.text += d[props[0]][props[1]];
							}
						} else {
							obj.text += d[l];
						}
					} else {
						obj.text += l;
					}
				});
			}
			return obj;
		});
	}
}
