import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class EventsService {
	public loginComplete$: EventEmitter<any>;

	constructor() {
		this.loginComplete$ = new EventEmitter();
	}
}
