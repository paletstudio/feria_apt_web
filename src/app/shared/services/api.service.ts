import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class APIService {
    headers: any;
    apiUrl: string = environment.apiUrl;

    constructor(private http: Http, private storage: LocalStorageService) {
    }

    public get(url: string): Observable<any> {
        this.headers = new Headers();
        this.headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
        return this.http.get(this.apiUrl + url, { headers: this.headers }).map(this.extractData).catch(this.handleError);
    }

    public post(url: string, data: any = {}): Observable<any> {
        this.headers = new Headers();
        this.headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
        return this.http.post(this.apiUrl + url, data, { headers: this.headers }).map(this.extractData).catch(this.handleError);
    }

    public put(url: string, data: any = {}): Observable<any> {
        this.headers = new Headers();
        this.headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
        return this.http.put(this.apiUrl + url, data, { headers: this.headers }).map(this.extractData).catch(this.handleError);
    }

    public delete(url: string): Observable<any> {
        this.headers = new Headers();
        this.headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
        return this.http.delete(this.apiUrl + url, { headers: this.headers }).map(this.extractData).catch(this.handleError);
    }

    public pdf(id: number) {
        window.open("http://158.69.202.20/hfs_tournament/services/public/api/pesaje/"+id+"/pdf?token="+this.storage.get('token'), "_blank");
    }

	private extractData(res: Response) {
		let body = res.json();
		return body || [];
	}

	private handleError(error: Response | any) {
        let body = error.json();
		return Observable.throw(body);
	}
}
