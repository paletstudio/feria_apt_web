import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from '../../components/login/login.service';
import { LoginComponent } from '../../components/login/login.component';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class RoutesActivate implements CanActivate {

	constructor(private loginService: LoginService, private router: Router) { }

	canActivate() {
		if (this.loginService.isLogged()) {
			return true;
		} else {
			this.router.navigate(['login']);
			return false;
		}
	}

}

@Injectable()
export class LoginActivate implements CanActivate{
	constructor(private loginService: LoginService, private router: Router) { }

	canActivate() {
		if (!this.loginService.isLogged()) {
			return true;
		} else {
			this.router.navigate(['dashboard']);
			return false;
		}
	}
}

@Injectable()
export class AdminRoutes implements CanActivate{
	private user: any;
	constructor(private loginService: LoginService, private router: Router, private storage: LocalStorageService) {
		this.user = storage.get('user');
	}

	canActivate() {
		if (this.user.profile_id === 1) {
			return true;
		} else {
			this.router.navigate(['dashboard']);
			return false;
		}
	}
}

@Injectable()
export class ClientRoutes implements CanActivate{
	private user: any;
	constructor(private loginService: LoginService, private router: Router, private storage: LocalStorageService) {
		this.user = storage.get('user');
	}

	canActivate() {
		if (this.user.profile_id === 2) {
			return true;
		} else {
			this.router.navigate(['dashboard']);
			return false;
		}
	}
}
