import { Injectable } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class UserService {
	constructor(private storage: LocalStorageService, private jwtHelper: JwtHelper) { }

	getCurrentUser(): any {
        let token = this.storage.get('token');
        let decodedToken = this.jwtHelper.decodeToken(token.toString());
        return decodedToken.user;
	}

    isClient(): boolean {
        let user = this.getCurrentUser();
        return user && user.client_id;
    }
}
