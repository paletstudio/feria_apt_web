import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../components/login/login.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { EventsService } from '../services/events.service';

@Component({
	selector: 'sidebar',
	templateUrl: './sidebar.html'
})
export class SidebarComponent implements OnInit {
	public profile: number;
	@Input() role;

	constructor(private loginService: LoginService, private storage: LocalStorageService, private router: Router, private events: EventsService) {
		// this.events.loginComplete$.subscribe(event => {
		// 	this.profile = event.profile_id;
		// });
	}

	ngOnInit(): void {}

	logout() {
		this.loginService.logout()
		.subscribe(() => {
            this.storage.clearAll();
            this.router.navigate(['login']);
        }, () => {
            this.storage.clearAll();
            this.router.navigate(['login']);
        });
	}
}
