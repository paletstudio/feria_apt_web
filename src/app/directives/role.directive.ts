import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Directive({ selector: '[inRole]' })
export class InRoleDirective {
	private user: any;

	constructor(
		private templateRef: TemplateRef<any>,
		private viewContainerRef: ViewContainerRef,
		private storage: LocalStorageService
	) {
		this.user = storage.get('user');
	}
	@Input()
	set inRole(roles: Array<number>) {
		this.viewContainerRef.clear(); // First clear the container
		if (roles.find((e) => {return e === this.user.profile_id})){
			this.viewContainerRef.createEmbeddedView(this.templateRef);
		} else {
			this.viewContainerRef.clear();
		}
	}
}
