export class Rate {
	id: number;
	nombre: string = '';
	precio: number = null;
	tipo: number = null;
	ac: boolean = false;
	activo: boolean = true;
	fecha_tarifa: string = '';
	fecha_fin: string = '';
	constructor(){
		this.nombre = '';
		this.precio = null;
		this.tipo = null;
		this.ac = false;
		this.activo = true;
		this.fecha_tarifa = '';
	}
}
