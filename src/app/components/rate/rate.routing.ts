import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RateComponent } from './rate.component';
import { RateFormComponent } from './rate-form/rate-form.component';

export const routableComponents = [
	RateComponent,
	RateFormComponent
];

const routes: Routes = [
	{
		path: '',
		component: RateComponent
	},
	{
		path: 'new',
		component: RateFormComponent
	},
	{
		path: ':id',
		component: RateFormComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class RateRoutingModule { }
