import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { RateService } from '../rate.service';
import { Rate } from '../rate';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';
import 'moment/locale/es';

@Component({
	selector: 'rate-from',
	templateUrl: 'rate-form.component.html',
})
export class RateFormComponent implements OnInit {
	public rate: Rate;
	public showDatePickerI: boolean = false;
	public formatedDateI: string;
	public datepickerConfigInit: {
		minDate: Date,
	};
	public showDatePickerE: boolean = false;
	public formatedDateE: string;
	public datepickerConfigEnd: {
		minDate: Date,
	};
	@ViewChild('form') form;
	@ViewChild('filedialog') filedialog: ElementRef;

	constructor(
		private rateService: RateService,
		private toastr: ToastrService,
		private route: ActivatedRoute,
		private router: Router
	) {
		moment.locale('es');
		this.datepickerConfigInit = {
			minDate: new Date
		};
		this.route.params.subscribe(
			params => {
				let id = +params['id'];
				if (id) {
					this.rateService.get(id).subscribe(
						res => {
							this.rate = res.data;
							this.formatedDateI = moment(this.rate.fecha_tarifa).format('dddd, D [de] MMMM [de] YYYY');
						}
					);
				}
			}
		);
		this.datepickerConfigEnd = {
			minDate: new Date
		}
	}

	ngOnInit() {
		this.rate = new Rate();
		console.log(this.rate);
	}

	activeDateChangeInitDate(event) {
		this.rate.fecha_tarifa = moment(event).format('YYYY-MM-DD');
		this.showDatePickerI = false;
		this.formatedDateI = moment(this.rate.fecha_tarifa).format('dddd, D [de] MMMM [de] YYYY');
		this.formatedDateE = '';
		this.rate.fecha_fin = null;
		this.datepickerConfigEnd.minDate = moment(this.rate.fecha_tarifa).toDate();
	}

	activeDateChangeEndDate(event) {
		this.rate.fecha_fin = moment(event).format('YYYY-MM-DD');
		this.showDatePickerE = false;
		this.formatedDateE = moment(this.rate.fecha_fin).format('dddd, D [de] MMMM [de] YYYY');
	}

	save() {
		// this.rate.imagen = 'path/to/image';
		if (!this.rate.id) {
			this.rateService.create(this.rate).subscribe(
				res => {
					this.toastr.success('Tarifa creada correctamente', 'Éxito');
					this.form.reset();
					setTimeout(() => {
						this.rate = new Rate();
					}, 100)
				}
			);
		} else {
			this.rateService.edit(this.rate).subscribe(
				res => {
					this.toastr.success('Tarifa editada correctamente', 'Éxito');
				}
			);
		}
	}

	delete() {
		this.rateService.delete(this.rate.id).subscribe(() => {
			this.toastr.success('Se eliminó la tarifa correctamente', 'Éxito');
			this.router.navigate(['rate']);
		});
	}

}
