import { Component, OnInit } from '@angular/core';
import { RateService } from './rate.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'rate',
	templateUrl: './rate.component.html',
	styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {
	public rate: any = {
		data: [],
		current_page: 1,
		per_page: 10,
		total: 10
	};
	static switch: boolean = false;

	constructor(
		private rateService: RateService,
		private toastr: ToastrService
	) {
		this.rateService.getAll().subscribe(res => {
			console.log(res);
			res.data.current_page--;
			this.rate = res.data
		})
	}

	ngOnInit() {
	}

	setPage(event) {
		this.rateService.getAll(event.offset + 1).subscribe(res => {
			res.data.current_page--;
			this.rate = res.data
		})
	}

	triggerActive(event: Event, rate) {
		// event.preventDefault();
		// event.stopPropagation();
		// swal({
		// 	title: '¿Está seguro?',
		// 	text: 'Se cambiará el aviso.',
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonText: 'Activar',
		// 	cancelButtonText: 'Cancelar',
		// 	confirmButtonColor: '#6ccf60',
 	// 		cancelButtonColor: '#20a8d8'
		// }).then( () => {
			rate.activo = !rate.activo;
			this.rateService.edit(rate).subscribe( res => {
				this.toastr.success('Se guardó correctamente la tarifa.', 'Éxito');
			});
		// }).catch( () => {});
	}

}
