import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RateRoutingModule, routableComponents } from './rate.routing';
import { RateService } from './rate.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UiSwitchModule } from 'ng2-ui-switch';
import { DatepickerModule } from 'ngx-bootstrap';
import { SweetAlertService } from 'ng2-sweetalert2';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RateRoutingModule,
		NgxDatatableModule,
		UiSwitchModule,
		CKEditorModule,
		DatepickerModule.forRoot(),
	],
	declarations: [routableComponents],
	providers: [
		RateService,
		SweetAlertService,
	]
})
export class RateModule { }
