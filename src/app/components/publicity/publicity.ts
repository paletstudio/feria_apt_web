export class Publicity {
	public descripcion:string = '';
	public fecha_inicio: string = '';
	public fecha_fin: string = '';
	public banner: string = '';
	public link: string = '';
	public activo: boolean = false;
	public zona_id: number = null;
	public id?: number;
}
