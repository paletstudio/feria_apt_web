import { Injectable } from '@angular/core';
import { APIService } from '../../shared/services/api.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

@Injectable()
export class PublicityService {
	public endpoint: string = 'publicity';
	constructor(
		private api: APIService
	) { }

	public getAll(): Observable<any> {
		return this.api.get(this.endpoint);
	}

	public get(id: number): Observable<any> {
		return this.api.get(this.endpoint + '/' + id);
	}

	public create(publicity: any): Observable<any> {
		return this.api.post(this.endpoint, publicity);
	}

	public edit(publicity: any): Observable<any> {
		return this.api.put(this.endpoint + '/' + publicity.id, publicity);
	}

	public delete(id: number): Observable<any> {
		return Observable.fromPromise(swal({
			title: '¿Está seguro?',
			text: 'Esta acción no se puede revertir.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Borrar',
			cancelButtonText: 'Cancelar',
			confirmButtonColor: '#ff5454',
 			cancelButtonColor: '#20a8d8',
		}))
		.flatMap(() => this.api.delete(this.endpoint + id));
	}
}
