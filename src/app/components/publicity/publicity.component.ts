import { Component, OnInit } from '@angular/core';
import { PublicityService } from './publicity.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'publicity',
	templateUrl: './publicity.component.html',
	styleUrls: ['./publicity.component.scss']
})
export class PublicityComponent implements OnInit {

	public publicity: Observable<any>;
    static switch: boolean = false;

    constructor(
		private publicityService: PublicityService,
		private toastr: ToastrService
    ) {
		this.publicity = this.publicityService.getAll().map(m => m.data);
    }

    ngOnInit() {
    }

    triggerActive(event: Event, publicity) {
		// event.preventDefault();
		// event.stopPropagation();
		// swal({
		// 	title: '¿Está seguro?',
		// 	text: 'Se cambiará el aviso.',
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonText: 'Activar',
		// 	cancelButtonText: 'Cancelar',
		// 	confirmButtonColor: '#6ccf60',
		// 		cancelButtonColor: '#20a8d8'
		// }).then( () => {
		publicity.activo = !publicity.activo;
		this.publicityService.edit(publicity).subscribe(res => {
			this.toastr.success('Se guardó correctamente la publicidad.', 'Éxito');
		});
		// }).catch( () => {});
    }

}
