import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicityComponent } from './publicity.component';
import { PublicityFormComponent } from './publicity-form/publicity-form.component';

export const routableComponents = [
	PublicityComponent,
	PublicityFormComponent
];

const routes: Routes = [
	{
		path: '',
		component: PublicityComponent
	},
	{
		path: 'new',
		component: PublicityFormComponent
	},
	{
		path: ':id',
		component: PublicityFormComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PublicityRoutingModule { }
