import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UiSwitchModule } from 'ng2-ui-switch';
import { DatepickerModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng2-select';
import { SweetAlertService } from 'ng2-sweetalert2';

import { PublicityRoutingModule, routableComponents } from './publicity.routing';
import { PublicityService } from './publicity.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		NgxDatatableModule,
		UiSwitchModule,
		PublicityRoutingModule,
		SelectModule,
		DatepickerModule.forRoot(),
	],
	declarations: [
		routableComponents
	],
	providers: [
		SweetAlertService,
		PublicityService,
	]
})
export class PublicityModule { }
