import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { PublicityService } from '../publicity.service';
import { Publicity } from '../publicity';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';
import 'moment/locale/es';
import { ZoneService } from '../../zone/zone.service';

@Component({
	selector: 'publicity-form',
	templateUrl: 'publicity-form.component.html',
	providers: [ZoneService]
})
export class PublicityFormComponent implements OnInit {
	public publicity: Publicity;
	public showDatePickerI: boolean = false;
	public formatedDateI: string;
	public datepickerConfigInit: {
		minDate: Date,
	};
	public showDatePickerE: boolean = false;
	public formatedDateE: string;
	public datepickerConfigEnd: {
		minDate: Date,
	};
	public zones: any;
	@ViewChild('form') form;
	@ViewChild('zone') zoneElement;
	@ViewChild('filedialog') filedialog: ElementRef;

	constructor(
		private publicityService: PublicityService,
		private toastr: ToastrService,
		private route: ActivatedRoute,
		private router: Router,
		private zone: ZoneService
	) {
		moment.locale('es');
		this.datepickerConfigInit = {
			minDate: new Date
		};
		this.datepickerConfigEnd = {
			minDate: this.datepickerConfigInit.minDate
		};
		this.zone.getAll().map(zone => {
			if(zone.data.length > 0) {
				zone.data.forEach( obj => {
					obj.text = obj.nombre;
				});
			}
			return zone;
		}).subscribe( zones => {
			this.zones = zones.data;
		});
		this.route.params.subscribe(
			params => {
				let id = +params['id'];
				if (id) {
					this.publicityService.get(id).subscribe(
						res => {
							this.publicity = res.data;
							let zone_current = this.zones.find( o => {
								return o.id = this.publicity.zona_id;
							});
							this.zoneElement.active.push(zone_current);
							this.formatedDateI = moment(this.publicity.fecha_inicio).format('dddd, D [de] MMMM [de] YYYY');
							this.formatedDateE = moment(this.publicity.fecha_fin).format('dddd, D [de] MMMM [de] YYYY');
						}
					);
				}
			}
		);
	}

	ngOnInit() {
		this.publicity = new Publicity;
	}

	activeDateChangeInitDate(event) {
		this.publicity.fecha_inicio = moment(event).format('YYYY-MM-DD');
		this.showDatePickerI = false;
		this.formatedDateI = moment(this.publicity.fecha_inicio).format('dddd, D [de] MMMM [de] YYYY');
		this.datepickerConfigEnd.minDate = moment(this.publicity.fecha_inicio).toDate();
	}

	activeDateChangeEndDate(event) {
		this.publicity.fecha_fin = moment(event).format('YYYY-MM-DD');
		this.showDatePickerE = false;
		this.formatedDateE = moment(this.publicity.fecha_fin).format('dddd, D [de] MMMM [de] YYYY');
	}

	save() {
		if (!this.publicity.id) {
			this.publicityService.create(this.publicity).subscribe(
				res => {
					this.publicity = new Publicity;
					this.toastr.success('Publicidad creada correctamente', 'Éxito');
					this.filedialog.nativeElement.innerHTML = 'Escoja una imágen...';
					this.zoneElement.active = [];
					this.form.reset();
				}
			);
		} else {
			this.publicityService.edit(this.publicity).subscribe(
				res => {
					this.toastr.success('Publicidad editada correctamente', 'Éxito');
				}
			);
		}
	}

	delete() {
		this.publicityService.delete(this.publicity.id).subscribe(() => {
			this.toastr.success('Se eliminó la publicidad correctamente', 'Éxito');
			this.router.navigate(['publicity']);
		});
	}

	changeListener(e): void {
		this.readThis(e.target);
		this.filedialog.nativeElement.innerHTML = e.target.files[0].name;
	}

	readThis(inputValue: any): void {
		var file: File = inputValue.files[0];
		var myReader: FileReader = new FileReader();

		myReader.onloadend = (e) => {
			this.publicity.banner = myReader.result;
		}
		myReader.readAsDataURL(file);
	}

	selectZone(event): void {
		this.publicity.zona_id = event.id;
	}
}
