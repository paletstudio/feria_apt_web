import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AdviceService } from '../advice.service';
import { Advice } from '../advice';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';
import 'moment/locale/es';

@Component({
	selector: 'advice-from',
	templateUrl: 'advice-form.component.html',
})
export class AdviceFormComponent implements OnInit {
	public advice: Advice;
	public showDatePickerI: boolean = false;
	public formatedDateI: string;
	public datepickerConfigInit: {
		minDate: Date,
	};
	@ViewChild('form') form;
	@ViewChild('filedialog') filedialog: ElementRef;

	constructor(
		private adviceService: AdviceService,
		private toastr: ToastrService,
		private route: ActivatedRoute,
		private router: Router
	) {
		moment.locale('es');
		this.datepickerConfigInit = {
			minDate: new Date
		};
		this.route.params.subscribe(
			params => {
				let id = +params['id'];
				if (id) {
					this.adviceService.get(id).subscribe(
						res => {
							setTimeout(() => {
								this.advice = res.data;
								this.formatedDateI = moment(this.advice.fecha).format('dddd, D [de] MMMM [de] YYYY');
							}, 100)
						}
					);
				}
			}
		);
	}

	ngOnInit() {
		this.advice = new Advice;
	}

	activeDateChangeInitDate(event) {
		this.advice.fecha = moment(event).format('YYYY-MM-DD');
		this.showDatePickerI = false;
		this.formatedDateI = moment(this.advice.fecha).format('dddd, D [de] MMMM [de] YYYY');
	}

	save() {
		// this.advice.imagen = 'path/to/image';
		if (!this.advice.id) {
			this.adviceService.create(this.advice).subscribe(
				res => {
					this.toastr.success('Aviso creado correctamente', 'Éxito');
					this.filedialog.nativeElement.innerHTML = 'Escoja una imágen...';
					this.form.reset();
					setTimeout(() => {
						this.advice = new Advice;
					}, 100)
				}
			);
		} else {
			this.adviceService.edit(this.advice).subscribe(
				res => {
					this.toastr.success('Aviso editado correctamente', 'Éxito');
				}
			);
		}
	}

	delete() {
		this.adviceService.delete(this.advice.id).subscribe(() => {
			this.toastr.success('Se eliminó el aviso correctamente', 'Éxito');
			this.router.navigate(['advice']);
		});
	}

	changeListener(e): void {
		this.readThis(e.target);
		this.filedialog.nativeElement.innerHTML = e.target.files[0].name;
	}

	readThis(inputValue: any): void {
		var file: File = inputValue.files[0];
		var myReader: FileReader = new FileReader();

		myReader.onloadend = (e) => {
			this.advice.imagen = myReader.result;
		}
		myReader.readAsDataURL(file);
	}
}
