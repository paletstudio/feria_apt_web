import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdviceComponent } from './advice.component';
import { AdviceFormComponent } from './advice-form/advice-form.component';

export const routableComponents = [
	AdviceComponent,
	AdviceFormComponent
];

const routes: Routes = [
	{
		path: '',
		component: AdviceComponent
	},
	{
		path: 'new',
		component: AdviceFormComponent
	},
	{
		path: ':id',
		component: AdviceFormComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AdviceRoutingModule { }
