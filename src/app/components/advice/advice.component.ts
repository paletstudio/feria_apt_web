import { Component, OnInit } from '@angular/core';
import { AdviceService } from './advice.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'advice',
	templateUrl: './advice.component.html',
	styleUrls: ['./advice.component.scss']
})
export class AdviceComponent implements OnInit {
	public advices: Observable<any>;
	static switch: boolean = false;

	constructor(
		private adviceService: AdviceService,
		private toastr: ToastrService
	) {
		this.advices = this.adviceService.getAll().map(m => m.data);
	}

	ngOnInit() {
	}

	triggerActive(event: Event, advice) {
		// event.preventDefault();
		// event.stopPropagation();
		// swal({
		// 	title: '¿Está seguro?',
		// 	text: 'Se cambiará el aviso.',
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonText: 'Activar',
		// 	cancelButtonText: 'Cancelar',
		// 	confirmButtonColor: '#6ccf60',
 	// 		cancelButtonColor: '#20a8d8'
		// }).then( () => {
			advice.activo = !advice.activo;
			this.adviceService.edit(advice).subscribe( res => {
				this.toastr.success('Se guardó correctamente el aviso.', 'Éxito');
			});
		// }).catch( () => {});
	}

}
