export class Advice {
	id?: number;
	titulo: string = '';
	subtitulo: string = '';
	imagen: string = '';
	descripcion: string = '';
	fecha: string = '';
	activo: boolean = true;
}
