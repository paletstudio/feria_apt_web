import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AdviceRoutingModule, routableComponents } from './advice.routing';
import { AdviceService } from './advice.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UiSwitchModule } from 'ng2-ui-switch';
import { DatepickerModule } from 'ngx-bootstrap';
import { SweetAlertService } from 'ng2-sweetalert2';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		AdviceRoutingModule,
		NgxDatatableModule,
		UiSwitchModule,
		CKEditorModule,
		DatepickerModule.forRoot(),
	],
	declarations: [routableComponents],
	providers: [
		AdviceService,
		SweetAlertService,
	]
})
export class AdviceModule { }
