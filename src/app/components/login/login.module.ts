import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { LoginRouteModule, routableComponents } from './login.route';
import { LoginService } from './login.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		LoginRouteModule,
	],
	declarations: [routableComponents],
	providers: [LoginService]
})
export class LoginModule { }
