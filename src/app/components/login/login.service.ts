import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { environment } from 'environments/environment';
import { User } from './user';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from 'angular-2-local-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginService {
	public endpoint: string = environment.apiUrl;
	constructor(private http: Http, private storage: LocalStorageService) { }

	login(user: User): Observable<any> {
		return this.http.post(this.endpoint + 'login', user).map(this.extractData).catch(this.handleError);
	}

	logout() {
		let headers = new Headers;
		headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
		return this.http.post(this.endpoint + 'logout', {}, {headers: headers}).map(this.extractData).catch(this.handleError)
	}

	isLogged() {
		return !!this.storage.get('token');
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || [];
	}

	private handleError(error: Response | any) {
        let body = error.json();
		return Observable.throw(body);
	}
}
