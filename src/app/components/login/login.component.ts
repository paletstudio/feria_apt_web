import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './user';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';
import { EventsService } from '../../shared/services/events.service';

@Component({
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	providers: [JwtHelper]
})
export class LoginComponent implements OnInit {
	public user: User;
	constructor(private loginService: LoginService, private router: Router, private toastr: ToastrService, private localStorage: LocalStorageService, private jwt: JwtHelper, private events: EventsService) {
		this.user = new User;
	}

	ngOnInit() { }

	login() {
		this.loginService.login(this.user)
			.subscribe(res => {
				let token = res.token;
				let decode = this.jwt.decodeToken(token);
				let user = decode.user;
				this.localStorage.set('token', token);
				this.localStorage.set('user', user);
				// this.events.loginComplete$.emit(user);
			},
			error => {
				this.toastr.error(error.exception, 'Error');
			},
			() => {
				this.router.navigate(['dashboard']);
			});
	}

}
