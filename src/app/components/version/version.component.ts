import { Component, OnInit } from '@angular/core';
import { VersionService } from './version.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'version',
	templateUrl: './version.component.html',
	styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit {

	public version:any;
    static switch: boolean = false;

    constructor(
		private versionService: VersionService,
		private toastr: ToastrService
    ) {
		this.version = {
			version: ''
		};
	}

    ngOnInit() {
		this.versionService.get().subscribe(res => {
			console.log(res);
			this.version.version = res.data.version;
		});
	}

	save() {
		this.versionService.edit(this.version).subscribe(res => {
			this.toastr.success('Versión actualizada correctamente', 'Éxito');
		}, error => {
			this.toastr.error('Se produjo un error, inténtelo más tarde.', 'Error');
		});
	}

}
