import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { UiSwitchModule } from 'ng2-ui-switch';

import { VersionRoutingModule, routableComponents } from './version.routing';
import { VersionService } from './version.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		UiSwitchModule,
		VersionRoutingModule
	],
	declarations: [
		routableComponents
	],
	providers: [
		VersionService
	]
})
export class VersionModule { }
