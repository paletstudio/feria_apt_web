import { Injectable } from '@angular/core';
import { APIService } from '../../shared/services/api.service';
import { Observable } from 'rxjs';

@Injectable()
export class VersionService {
	public endpoint: string = 'version';
	constructor(
		private api: APIService
	) { }

	public get(): Observable<any> {
		return this.api.get(this.endpoint);
	}

	public edit(version: any): Observable<any> {
		return this.api.put(this.endpoint, version);
	}

}
