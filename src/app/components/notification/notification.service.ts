import { Injectable } from '@angular/core';
import { APIService } from '../../shared/services/api.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

@Injectable()
export class NotificationService {
	public endpoint: string = 'notification';
	constructor(
		private api: APIService
	) { }

	public create(notification: any): Observable<any> {
		return this.api.post(this.endpoint, notification);
	}

}
