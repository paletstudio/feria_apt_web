import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NotificationRoutingModule, routableComponents } from './notification.routing';
import { NotificationService } from './notification.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		NotificationRoutingModule
	],
	declarations: [
		routableComponents
	],
	providers: [
		NotificationService
	]
})
export class NotificationModule { }
