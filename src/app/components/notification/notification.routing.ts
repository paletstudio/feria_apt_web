import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationComponent } from './notification.component';

export const routableComponents = [
	NotificationComponent,
];

const routes: Routes = [
	{
		path: '',
		component: NotificationComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class NotificationRoutingModule { }
