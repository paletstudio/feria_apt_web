import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from './notification.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

	public notification: any;
	@ViewChild('form') form;

    constructor(
		private notificationService: NotificationService,
		private toastr: ToastrService
    ) {}

	ngOnInit() {
		this.initNotification();
	}

	initNotification() {
		this.notification = {
			message: ''
		};
	}

    send() {
		this.notificationService.create(this.notification).subscribe(res => {
			this.toastr.success('Éxito', 'Se ha enviado la notificación correctamente');
			this.initNotification();
			this.form.reset();
		}, error => {
			console.log(error);
			this.toastr.error('Error', 'Se produjo un error, inténtelo más tarde.');
		});
	}

}
