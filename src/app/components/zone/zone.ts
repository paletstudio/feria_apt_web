export class Zone {
	public id?: number;
	public nombre: string = '';
	public descripcion: string = '';
	public alto: string = '';
	public ancho: string = '';
	public costo: number = null;
	public activo: boolean = false;
}
