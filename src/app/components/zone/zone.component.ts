import { Component, OnInit } from '@angular/core';
import { ZoneService } from './zone.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'zone',
	templateUrl: './zone.component.html',
	styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {

	public zones: Observable<any>;
    static switch: boolean = false;

    constructor(
		private zoneService: ZoneService,
		private toastr: ToastrService
    ) {
		this.zones = this.zoneService.getAll().map(m => m.data);
    }

    ngOnInit() {
    }

    triggerActive(event: Event, zone) {
		// event.preventDefault();
		// event.stopPropagation();
		// swal({
		// 	title: '¿Está seguro?',
		// 	text: 'Se cambiará el aviso.',
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonText: 'Activar',
		// 	cancelButtonText: 'Cancelar',
		// 	confirmButtonColor: '#6ccf60',
		// 		cancelButtonColor: '#20a8d8'
		// }).then( () => {
		zone.activo = !zone.activo;
		this.zoneService.edit(zone).subscribe(res => {
			this.toastr.success('Se guardó correctamente la zona.', 'Éxito');
		});
		// }).catch( () => {});
    }

}
