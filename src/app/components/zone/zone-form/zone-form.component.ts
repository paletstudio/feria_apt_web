import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ZoneService } from '../zone.service';
import { Zone } from '../zone';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';
import 'moment/locale/es';

@Component({
	selector: 'zone-form',
	templateUrl: './zone-form.component.html',
})
export class ZoneFormComponent implements OnInit {
	public zone: Zone;
	@ViewChild('form') form;

	constructor(
		private zoneService: ZoneService,
		private toastr: ToastrService,
		private route: ActivatedRoute,
		private router: Router
	) {
		this.route.params.subscribe(
			params => {
				let id = +params['id'];
				if (id) {
					this.zoneService.get(id).subscribe(
						res => {
							this.zone = res.data;
						}
					);
				}
			}
		);
	}

	ngOnInit() {
		this.zone = new Zone;
	}

	save() {
		if (!this.zone.id) {
			this.zoneService.create(this.zone).subscribe(
				res => {
					this.zone = new Zone;
					this.toastr.success('Zona creada correctamente', 'Éxito');
					this.form.reset();
				}
			);
		} else {
			this.zoneService.edit(this.zone).subscribe(
				res => {
					this.toastr.success('Zona editada correctamente', 'Éxito');
				}
			);
		}
	}

	delete() {
		this.zoneService.delete(this.zone.id).subscribe(() => {
			this.toastr.success('Se eliminó la zona correctamente', 'Éxito');
			this.router.navigate(['zone']);
		});
	}

}
