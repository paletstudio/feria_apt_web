import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UiSwitchModule } from 'ng2-ui-switch';
import { DatepickerModule } from 'ngx-bootstrap';
import { SweetAlertService } from 'ng2-sweetalert2';

import { ZoneRoutingModule, routableComponents } from './zone.routing';
import { ZoneService } from './zone.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		NgxDatatableModule,
		UiSwitchModule,
		ZoneRoutingModule,
		DatepickerModule.forRoot(),
	],
	declarations: [
		routableComponents
	],
	providers: [
		SweetAlertService,
		ZoneService,
	]
})
export class ZoneModule { }
