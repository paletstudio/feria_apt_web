import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoneComponent } from './zone.component';
import { ZoneFormComponent } from './zone-form/zone-form.component';

export const routableComponents = [
	ZoneComponent,
	ZoneFormComponent
];

const routes: Routes = [
	{
		path: '',
		component: ZoneComponent
	},
	// {
	// 	path: 'new',
	// 	component: ZoneFormComponent
	// },
	{
		path: ':id',
		component: ZoneFormComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ZoneRoutingModule { }
