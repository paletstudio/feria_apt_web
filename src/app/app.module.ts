import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule, AccordionModule, AccordionComponent } from 'ngx-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { APIService } from './shared/services/api.service';
import { UserService } from './shared/services/user.service';
import { LocalStorageModule, ILocalStorageServiceConfig } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';

// Routing Module
import { AppRoutingModule } from './app.routing';
import { RoutesActivate, LoginActivate, AdminRoutes, ClientRoutes } from './shared/services/auth.service';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';

import { LoginModule } from './components/login/login.module';
import { EventsService } from './shared/services/events.service';
import { InRoleDirective } from './directives/role.directive';

const storageConfig: ILocalStorageServiceConfig = {
	prefix: 'feria_apt',
	storageType: 'localStorage'
};

@NgModule({
	imports: [
		BrowserModule,
		HttpModule,
		BsDropdownModule.forRoot(),
		TabsModule.forRoot(),
		BrowserAnimationsModule,
		ToastrModule.forRoot(),
		AccordionModule.forRoot(),
		AppRoutingModule,
		LoginModule,
		LocalStorageModule.withConfig(storageConfig),
	],
	declarations: [
		AppComponent,
		FullLayoutComponent,
		SidebarComponent,
		NAV_DROPDOWN_DIRECTIVES,
		BreadcrumbsComponent,
		SIDEBAR_TOGGLE_DIRECTIVES,
		AsideToggleDirective,
		InRoleDirective,
	],
	providers: [
		{
			provide: LocationStrategy,
			useClass: HashLocationStrategy
		},
		APIService,
		RoutesActivate,
		LoginActivate,
		AdminRoutes,
		ClientRoutes,
		AccordionComponent,
		EventsService,
        JwtHelper,
        UserService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
