import { Component, OnInit } from '@angular/core';
import { APIService } from '../shared/services/api.service';
import { UserService } from 'app/shared/services/user.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './full-layout.component.html',
    providers: [UserService]
})
export class FullLayoutComponent implements OnInit {
	public expiringLicenses;

	constructor(private api: APIService) {

    }

	public disabled: boolean = false;
	public status: { isopen: boolean } = { isopen: false };

	public toggled(open: boolean): void {
	}

	public toggleDropdown($event: MouseEvent): void {
		$event.preventDefault();
		$event.stopPropagation();
		this.status.isopen = !this.status.isopen;
	}

	ngOnInit(): void {
	}
}
